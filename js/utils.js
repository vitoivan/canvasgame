const addListeners = () => {
    document.addEventListener('keydown', map.keydown);
    document.addEventListener('keyup', map.keyup);
    map.canvas.addEventListener('mousemove', map.updateMouseCoordinate.bind(map));
    map.canvas.addEventListener('mousedown', map.mouseDown.bind(map))
    map.canvas.addEventListener('mouseup', map.mouseUp.bind(map));
}
window.addEventListener('contextmenu', function (e) { 
    // do something here... 
    e.preventDefault(); 
});
