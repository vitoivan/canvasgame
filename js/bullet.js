class Bullet{
    constructor(x,y){
        this.radius = 5;
        this.x = x;
        this.y = y;
        const angle = Math.atan2(map.mouseY - this.y,  map.mouseX - this.x);
        this.dx = Math.cos(angle);
        this.dy = Math.sin(angle);
        this.speed = 4;
    }
    draw(){
        ctx.beginPath();
        ctx.arc(this.x,this.y,this.radius,0,Math.PI*2,false);
        ctx.fillStyle = '#fff';
        ctx.fill();
    }
    update(){
        this.draw();
        this.x += this.dx * this.speed;
        this.y += this.dy * this.speed;
        
    }

}


class ShotGunBullet extends Bullet{
    constructor(x,y){
        super(x,y);
        if(Math.random() > 0.5){
            this.mouseX = map.mouseX + (Math.random() * 50) + 10;
        }else{
            this.mouseX = map.mouseX - (Math.random() * 50) + 10;
        }

        if(Math.random() > 0.5){
            this.mouseY = map.mouseY + (Math.random() * 50) + 10;
        }else{
            this.mouseY = map.mouseY - (Math.random() * 50) + 10;
        }
        const angle = Math.atan2(this.mouseY - this.y, this.mouseX - this.x);
        this.dx = Math.cos(angle);
        this.dy = Math.sin(angle);
    }

    update(){
        this.draw();
        this.x += this.dx * this.speed;
        this.y += this.dy * this.speed;
    }
}