class Person{
    constructor(x,y, radius,life,color,lifeBarColor='#5f5'){
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.life = life;
        this.canShot = true;
        this.color = color;
        this.lifeBarColor = lifeBarColor;
    }

    updateLifeBar(){
        this.drawLifeBar();
    }
    drawLifeBar(){
        ctx.fillStyle = this.lifeBarColor;
        const lifeBarX = this.x - (this.radius * 2);
        const lifeBarY = this.y - (this.radius * 2.5)
        const size = (this.radius * 4) * (this.life/100);
        ctx.fillRect(lifeBarX, lifeBarY, size, (this.radius/2) + 5);
        
    }

}