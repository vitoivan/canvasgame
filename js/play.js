let map = new Map();
let ctx = map.ctx;
let enemys = [];
let intervalId = 0;
let player = new Player(innerWidth/2,innerHeight/2, 15);
let animationFrame = 0;

const animate = () => {
    animationFrame = requestAnimationFrame(animate);
    map.clearCtx();
    player.update();

    enemys.forEach( enemy => {
        enemy.update()
    })
    map.drawCrossHair();
    map.updateScore();
}
const init = () => {
    cancelAnimationFrame(animationFrame);
    ctx = map.ctx;
    enemys = [];
    clearInterval(intervalId);
    map.score = 0;
    for(let i = 0; i < 10; i++){
        enemys.push(new Enemy(15, 100,'red', '#ff9500'))
    }
    intervalId = setInterval( () => {
        enemys.push(new Enemy(15, 100, 'red', '#ff9500'))
    },1000);
  
    map.canvas.width = innerWidth;
    map.canvas.height = innerHeight;
    player = new Player(innerWidth/2,innerHeight/2, 15);
    animate();
}
init();
addListeners();