class Enemy extends Person{
    constructor(radius,life,color='#f55442', lifebarColor){
        super(0,0,radius,life, color, lifebarColor)
 
        if(Math.random() < 0.5){
            this.x = Math.random() < 0.5 ? - this.radius : innerWidth + this.radius;  
            this.y = Math.random() * innerHeight;
            
        }else{
            this.y = Math.random() < 0.5 ? - this.radius : innerHeight + this.radius;  
            this.x = Math.random() * innerWidth;
        }

        this.angle = Math.atan2(player.y - this.y, player.x - this.x);
        this.dx = Math.cos(this.angle)
        this.dy = Math.sin(this.angle)
        
        
    }

    draw(){
        ctx.beginPath();
        ctx.arc(this.x, this.y, this.radius, 0, 2*Math.PI, false);
        ctx.fillStyle = this.color;
        ctx.fill();
   
  
    }
    update(){
        this.followPlayer();
        this.draw();
        this.drawLifeBar();
    }
    followPlayer(){
        this.angle = Math.atan2(player.y - this.y, player.x - this.x);
        this.dx = Math.cos(this.angle)
        this.dy = Math.sin(this.angle)
        this.x += this.dx;
        this.y += this.dy;
    }
}
