class Player extends Person{
    constructor(x, y,radius=25){
        super(x,y,radius,100, '#fff')
        this.possibleKeys = {};
        this.speed = 2;
        this.weapons = ['pistol','shotgun', 'rifle'];
        this.selectedWeapon = 0;
        this.shooted = [];
        this.maxBullets = {
            pistol: 20,
            shotgun: 8,
            rifle: 50
        }
        this.bulletsRemaining = {
            rifle : this.maxBullets.rifle,
            pistol : this.maxBullets.pistol,
            shotgun : this.maxBullets.shotgun
        }
        this.trigger = false;
        this.reloading = {
            pistol: false,
            shotgun: false,
            rifle: false
        }
        this.pistolDelayReload = 500;
        this.rifleDelayReload = 3000;
        this.shotgunDelayReload = 3500
        this.timerPistol = Math.PI * 2 / this.pistolDelayReload;
        this.timerShot =  Math.PI * 2 / this.rifleDelayReload;
        this.timerRifle =  Math.PI * 2 / this.shotgunDelayReload;
        this.tempo = 0;
    }
    selfDraw(){
        ctx.beginPath();
        ctx.arc(this.x, this.y, this.radius, 0, 2*Math.PI, false);
        ctx.fillStyle = this.color;
        ctx.fill();
    }
    update(){
        this.updatePlayerPos();
        this.drawLifeBar();
        this.selfDraw();
        this.updateDeleteBullet();
        this.updateShot();
        this.qtdBullets();
        this.weaponIcon();
        this.drawReloadAnim();  
        
    }

    updatePlayerPos(){    

        for(let i in this.possibleKeys){
            if(this.possibleKeys[i] === true){
             
                if((i === 'w'|| i === 'W') && this.canMoveTop() === true){
                    this.y -= this.speed;
          
                }
                if((i === 's' || i === 'S') && this.canMoveBot() === true){
                    this.y += this.speed;
                }
                if((i === 'a' || i === 'A') && this.canMoveLeft() === true){
                    this.x -= this.speed;
                }
                if((i === 'd'|| i === 'D') && this.canMoveRight() === true){
                    this.x += this.speed;
                }
    
            }
        }
        enemys.forEach ( enemy => {
            const dist = Math.hypot((this.x - enemy.x),(this.y - enemy.y) );
        
            if(dist - (this.radius + enemy.radius) < 1){
                if(enemy.x > this.x){
                    this.x -= 20;
                    enemy.x += 20;
                }else{
                    this.x += 20;
                    enemy.x -= 20;
                }
                if(enemy.y > this.y){
                    this.y -= 20;
                    enemy.y += 20;
                }else{
                    this.y += 20;
                    enemy.y -= 20;
                }

                if(this.life - 40 < 1){
                    this.life = 0;
                    cancelAnimationFrame(animationFrame)
                }else{
                    this.life -= 40;
                }
            }
        })
    }
    move(key){
        if(!this.possibleKeys[key]){
            this.possibleKeys[key] = true;
        }
    }
    stopMove(key){
        this.possibleKeys[key] = false;
    }
    canMoveLeft(){
        const leftSide = this.x - this.radius;
        if(leftSide < 0){
            return false;
        }
        return true;
    }
    canMoveRight(){
        const rightSide = this.x + this.radius;
        if(rightSide > innerWidth){
            return false;
        }
        return true;
    }
    canMoveTop(){
        const topSide = this.y - this.radius;
        if(topSide < 0){
            return false;
        }
        return true;
    }
    canMoveBot(){
        const botSide = this.y + this.radius;
        if(botSide > innerHeight){
            return false;
        }
        return true;
    }
    updateShot(){
        const weapon = this.weapons[this.selectedWeapon];
        
        if(this.canShot === true && this.trigger === true && this.reloading[weapon] === false){     
            // if(this.bulletsRemaining[weapon] <= 0 ){
            //     this.reload();
            // }
            if(weapon === 'pistol' && this.bulletsRemaining[weapon] > 0){
                this.shooted.push(new Bullet(this.x,this.y));
                this.bulletsRemaining.pistol--;
            }
            if(weapon === 'shotgun' && this.bulletsRemaining[weapon] > 0){
                for(let i = 0; i < 8; i++){
                    this.shooted.push(new ShotGunBullet(this.x,this.y));
                }
                this.bulletsRemaining.shotgun -= 1;
            }
            if(weapon === 'rifle' && this.bulletsRemaining[weapon] > 0){
                this.shooted.push(new Bullet(this.x,this.y));
                this.bulletsRemaining.rifle--;
            }
                this.fireRateControl();
        }
    }

    fireRateControl(){
        const weapon = player.weapons[player.selectedWeapon]
        if(this.canShot === true && this.reloading[weapon] === false){
             player.canShot = false;
          
             if(weapon === 'pistol'){
                 setTimeout( () => {
                     player.canShot = true;
                 },450)
             }
        
             if(weapon === 'shotgun'){
                 setTimeout( () => {
                     player.canShot = true;
                 },1000)
             }
             if(weapon === 'rifle'){
                 setTimeout( () => {
                     player.canShot = true;
                 },50)
             }
             
         }
     }

    updateDeleteBullet(){
        this.shooted.forEach( (bullet,i) => {
            if(bullet.x < 0 || bullet.x > innerWidth || bullet.y < 0 || bullet.y > innerHeight){
                this.shooted.splice(i,1);
            }

            enemys.forEach( (enemy, enemyInd) => {
                const dist = Math.hypot((bullet.x - enemy.x), (bullet.y - enemy.y));
        
                if(dist - (enemy.radius + bullet.radius) < 1){
                    this.shooted.splice(i,1);
                    let damage = 0;
                    if(this.weapons[this.selectedWeapon] === 'pistol'){
                        damage = 40;
                    }
                    if(this.weapons[this.selectedWeapon] === 'rifle'){
                        damage = 25;
                    }
                    if(this.weapons[this.selectedWeapon] === 'shotgun'){
                        damage = 50;
                    }

                    if(enemy.life - damage < 1){
                        enemys.splice(enemyInd, 1);
                        map.score += 10;
                       
                    }else{
                        enemy.life -= damage;
                    }
                }
               
            }) 
            bullet.update();
        })
    }
    reload(){
        
        const weapon = this.weapons[this.selectedWeapon]
        if(this.bulletsRemaining[weapon] < this.maxBullets[weapon]){
            if(this.reloading[weapon] === false){
                this.reloading[weapon] = true;
               
                if(weapon === 'pistol'){
                    this.timerPistol = (Math.PI * 2) / this.pistolDelayReload
                    for(let i = 0; i < this.pistolDelayReload; i++){
                        setTimeout(() => {
                            this.timerPistol += (Math.PI * 2) / this.pistolDelayReload
                        },i)
                    }
                setTimeout(() => {
                    this.bulletsRemaining.pistol = this.maxBullets.pistol;
                    this.reloading[weapon] = false;
                    },this.pistolDelayReload)
    
                }
                if(weapon === 'shotgun'){
                    this.timerShot = (Math.PI * 2) / this.shotgunDelayReload
                    for(let i = 0; i < this.shotgunDelayReload; i++){
                        setTimeout(() => {
                            this.timerShot += (Math.PI * 2) / this.shotgunDelayReload
                        },i)
                    }
                setTimeout(() => {
                    this.bulletsRemaining.shotgun = this.maxBullets.shotgun;
                    this.reloading[weapon] = false;
                    },this.shotgunDelayReload)
    
                }
                if(weapon === 'rifle'){
                    this.timerRifle = (Math.PI * 2) / this.rifleDelayReload
                    for(let i = 0; i < this.rifleDelayReload; i++){
                        setTimeout(() => {
                            this.timerRifle += (Math.PI * 2) / this.rifleDelayReload
                        },i)
                    }
                setTimeout(() => {
                    this.bulletsRemaining.rifle = this.maxBullets.rifle;
                    this.reloading[weapon] = false;
                    },this.rifleDelayReload)
                }
            }
        }
        
    }
    swapWeapon(key){
        key = key.toLowerCase();
        if(key === 'e'){
            if(this.selectedWeapon < this.weapons.length - 1){
                this.selectedWeapon++;
            }else{
                this.selectedWeapon = 0;
            }
        }
        if(key === 'q'){
            if(this.selectedWeapon > 0){
                this.selectedWeapon--;
            }else{
                this.selectedWeapon = this.weapons.length - 1;
            }
        }
    }
    
    qtdBullets(){
        ctx.beginPath()
        ctx.fillStyle = '#f0f';
        ctx.fillRect(player.x - 44, player.y - 25,10,-this.bulletsRemaining[this.weapons[this.selectedWeapon]])
        ctx.fill();
        ctx.closePath();
    }
    weaponIcon(){
        const weapon = this.weapons[this.selectedWeapon];
        let img = null;
        if(weapon === 'pistol'){
            img = document.querySelector('img[src="./assets/pistol.png"]')
        }
        if(weapon === 'shotgun'){
            img = document.querySelector('img[src="./assets/shotgun.png"]')
        }
        if(weapon === 'rifle'){
            img = document.querySelector('img[src="./assets/ak.png"]')
        }
        
        ctx.drawImage(img, this.x + this.radius * 2,this.y - this.radius * 3, 37.5,25);
    }

    drawReloadAnim(){
      
        const radius = this.radius/2;

        if(this.reloading.pistol === true){
            ctx.beginPath();
            ctx.fillStyle = '#f00';
            const x = this.x + this.radius * 2 + 45;
            const y = this.y - this.radius * 2;
            ctx.arc(x, y, radius, 0, this.timerPistol);
            ctx.fill();
        }

        if(this.reloading.shotgun === true){
            ctx.beginPath();
            ctx.fillStyle = '#0f0';
            const x = this.x + this.radius * 2 + 45;
            const y = this.y - this.radius;
            ctx.arc(x, y, radius, 0, this.timerShot);
            ctx.fill();
        }
   
        if(this.reloading.rifle === true){
            ctx.beginPath();
            ctx.fillStyle = '#00f';
            const x = this.x + this.radius * 2 + 45;
            const y = this.y;
            ctx.arc(x, y, radius, 0, this.timerRifle);
            ctx.fill();
        }
    }
    
}