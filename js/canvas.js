class Map{
    constructor(){
        this.canvas = document.querySelector('canvas');
        this.canvas.width = innerWidth;
        this.canvas.height = innerHeight;
        this.ctx = this.canvas.getContext('2d');
        this.mouseX = 0;
        this.mouseY = 0;
        this.img = document.querySelector('img');
        this.score = 0;
    }
    keydown(e){
        player.move(e.key.toLowerCase());
        if(e.key === 'r' || e.key === 'R'){
            player.reload();
        }
        if(e.key === 'q' || e.key === 'Q' || e.key === 'e' || e.key === 'E'){
            player.swapWeapon(e.key);
        }
        if(e.key === 'z' || e.key === 'Z'){
            init();
        }
    }
    mouseDown(e){
        this.updateMouseCoordinate(e);
        if(e.button === 0){
            player.trigger = true;
        }
    }
    mouseUp(e){
        if(e.button === 0){
            player.trigger = false;
        }
    }
    keyup(e){
        player.stopMove(e.key.toLowerCase());
    }
    clearCtx(){
        this.ctx.clearRect(0,0, innerWidth, innerHeight);
        this.ctx.fillStyle = 'rgba(0,0,0, 0.1)';
        // this.ctx.drawImage(this.img,0,0, innerWidth, innerHeight);
        this.ctx.fillRect(0,0,innerWidth,innerHeight);
    }

    updateMouseCoordinate(e){  
        this.mouseX = e.clientX;
        this.mouseY = e.clientY;
    }
    drawCrossHair(){
        /*Left Cross*/
    
        this.ctx.fillStyle = '#fff';
        this.ctx.fillRect(this.mouseX - 2, this.mouseY, -5,1);
        /*Right Cross*/
        this.ctx.fillRect(this.mouseX + 2, this.mouseY, 5,1);
        /*Top Cross*/
        this.ctx.fillRect(this.mouseX - 0.7, this.mouseY - 1, 1,-5);
        /*Bot Cross*/
        this.ctx.fillRect(this.mouseX - 0.7, this.mouseY + 2, 1,5);
    }

    updateScore(){
        const div = document.querySelector('.score')
        div.innerText = this.score;
    }
}

